class Car{
    var gps: String=""
    var numberOfSeats: Int = 0


    override fun toString(): String {
        return "gps: ${this.gps}\nnumber of seats: ${this.numberOfSeats}  "
    }


}

class Manual {
    var userManual: String= ""

    override fun toString(): String {
        return this.userManual
    }
}

interface Builder{
    fun reset()
    fun setSeats(numSeats:String)
    fun setGPS(gps:String)
}

class  CarBuilder: Builder {
    private var car =  Car()

    override fun reset() {

        this.car = Car()
    }

    override fun setSeats(numSeats: String) {

        this.car.numberOfSeats = numSeats.toInt()
    }

    override fun setGPS(gps: String) {
        this.car.gps = gps
    }

    fun getProduct():Car {
        val product = this.car
        this.reset()
        return product
    }

}


class CarManalBuilder: Builder {
    private var manual: Manual  =Manual()

    override fun reset() {
        this.manual = Manual()
    }

    override fun setSeats(numSeats: String) {
        //add seats the instructions
        this.manual.userManual += "Manual Seats: total seats is  "+ numSeats+"\n"
   }

    override fun setGPS(gps: String) {
        // add gps instructions
        this.manual.userManual += "Manual gps is: " +gps

    }

    fun getProduct():Manual {
        val product = this.manual
        this.reset()
        return product
    }


}

class Director{
    private var builder:Builder? = null

    fun setBuilder(builder:Builder){
        this.builder = builder
    }

    fun constructSportsCar(builder: Builder){

        builder.reset()
        builder.setSeats("2")
        builder.setGPS("GPS_SPORTS")

    }

    fun constructLadaCar(builder: Builder){
        builder.reset()
        builder.setSeats("4")
        builder.setGPS("GPS_LADA")
    }

}

fun main(){
    var director: Director  = Director()
    var carBuilder: CarBuilder = CarBuilder()
    director.constructSportsCar(carBuilder)
    var car: Car = carBuilder.getProduct()

    println(car)

    var manualBuilder: CarManalBuilder = CarManalBuilder()
    director.constructSportsCar(manualBuilder)
    val manual: Manual = manualBuilder.getProduct()
    println("Car manual ")
    println(manual)

    println("creandon un lada XD")

    director.constructLadaCar(carBuilder)
    var lada = carBuilder.getProduct()
    println(lada)
    println("Manual del lada")
    director.constructLadaCar(manualBuilder)
    var ladaManual = manualBuilder.getProduct()
    println(ladaManual)


}