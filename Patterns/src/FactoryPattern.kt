import java.lang.Exception
/*
open  class Dialog{
    open fun createButton():Any{
        return throw Exception("Not Implement yet")
    }
}*/
abstract  class Dialog{
    abstract  fun createButton()
}
class WindowsDialog:Dialog(){
    override fun createButton() {
        val botonWindows =  WindowsButton()
        botonWindows.render()
        botonWindows.onClick()

    }
}


class WebDialog:Dialog(){
    override fun createButton(){
        val htmlButton =  HTMLButton()
        htmlButton.render()
        htmlButton.onClick()
    }
}

interface  Button {

    fun render()
    fun onClick()

}

class  WindowsButton: Button{
    override fun render() {
        println("Se renderiza el button en estilo windows")
    }

    override fun onClick() {
        println("Evento click en el boton XD ")
    }
}


class HTMLButton:Button{
    override fun render() {
        print("se renderiza un boton a estilo html")
    }

    override fun onClick() {
        print("Evento click en el Boton XD")
    }
}

class Application(var userInput:String
) {

    fun initialize(){

        when(userInput)
        {
            "Windows" ->  WindowsDialog().createButton()
            "Web" ->   WebDialog().createButton()
            else -> throw  Exception("No se ha implementado aun ")
        }

    }

}


fun main(){

    println("Entra el tipo de boton que quieres: Web o Windows")
    val tipoBoton = readLine()

    val app = Application(tipoBoton.toString())
    app.initialize()


    //botton.render()
    //botton.onClick()
}
