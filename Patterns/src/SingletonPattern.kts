class Database private constructor() {


    companion object {
        private var INSTANCE: Database? = null

        public fun getInstance(): Database?{
            if(INSTANCE==null){
                INSTANCE = Database()
                println("creating the new datatabase conn")
            }else{
                println("database already created")
            }
            return INSTANCE
        }
    }
}

fun main() {
    var foo: Database? = Database.getInstance()
    var bar: Database? = Database.getInstance()
    var bar2: Database? = Database.getInstance()
}