##Deber 1
Implementar el Factory Method en Kotlin

la solución se encuentra dentro de la carpeta Patterns/src con nombre FactoryPattern y es basado en el pseudocódigo que se encuentra en la página [refactoring guru](https://refactoring.guru/design-patterns/factory-method)

##DEBER 2 

Implementar el Builder Pattern en Kotlin

la solución se encuentra dentro de la carpeta Patterns/src con nombre BuilderPattern.kt y es basado en el 
pseudocódigo que se encuentra en la página [refactoring guru](https://refactoring.guru/design-patterns/builder) 

## DEBER 3

Implementar el Singleton Pattern en Kotlin 

la solución se encuentra dentro de la carpeta Patterns/src con nombre SingletonPatter.kt y es basado en el 
pseudocódigo que se encuentra en la página [refactoring guru](https://refactoring.guru/design-patterns/singleton) 



## Deber 4 

Decorator pattern implementado 

## Deber 5

observer pattern implementado





